# **crawler** [![tyoui](https://gitee.com/tyoui/logo/raw/master/logo/photolog.png)](http://blog.jtyoui.com)



## 这个是一个单机版的爬虫。开发简单。上手容易。只需要1分钟就可以搭建好,下载之后只需配置crawler.properties即可

[![](https://travis-ci.org/zhangyingwei/cockroach.svg?branch=master)]()
[![](https://img.shields.io/badge/language-java-orange.svg)]()
[![](https://img.shields.io/badge/jdk-1.8-green.svg)]()
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)


## 可以用最新maven repository下载
    
        <!-- https://mvnrepository.com/artifact/cn.tyoui/Crawler -->
        <dependency>
            <groupId>cn.tyoui</groupId>
            <artifactId>Crawler</artifactId>
            <version>1.8.5</version>
        </dependency>

### 详细步骤请看图
[![](https://gitee.com/tyoui/logo/raw/master/crawlerStep/1.png)]()      
[![](https://gitee.com/tyoui/logo/raw/master/crawlerStep/2.png)]()  
[![](https://gitee.com/tyoui/logo/raw/master/crawlerStep/3.png)]()  


## 配置crawler.properties(放在Maven resources下)
* [点击查看详细配置](./src/main/resources/crawler.properties)


### 配置完毕之后启动

    public class TestCrawler {
        public static void main(String[] args) throws Exception {
            new Crawler().start();
        }
    }
    