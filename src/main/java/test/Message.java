package test;

public class Message {
    private String name;
    private String address;
    private String tel;
    private String rank;
    private String money;
    private String url;

    public Message() {
    }

    public Message(String name, String address, String tel, String rank, String money, String url) {
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.rank = rank;
        this.money = money;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getTel() {
        return tel;
    }

    public String getRank() {
        return rank;
    }

    public String getMoney() {
        return money;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "name=" + name + "\taddress=" + address + "\ttel=" + tel + "\trank=" + rank + "\tmoney=" + money + "\turl=" + url;
    }
}
