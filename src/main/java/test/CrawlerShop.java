package test;

import cn.tyoui.httpclient.HttpCrawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class CrawlerShop {
    public static void main(String[] args) throws Exception {
        File file = new File("D:\\ideaCode\\Crawler\\src\\main\\java\\test\\shop.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        HttpCrawler httpCrawler = new HttpCrawler();
        httpCrawler.setDir("D://shop");
        while (bufferedReader.ready()) {
            String s = bufferedReader.readLine();
            httpCrawler.startCrawler(s, 0, 0);
        }
    }
}
