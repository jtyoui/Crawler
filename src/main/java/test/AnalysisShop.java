package test;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class AnalysisShop {
    public static void main(String[] args) throws Exception {
        List<Message> list = new ArrayList<>();
        File[] files = new File("D://shop").listFiles();
        int i = 0;
        for (File file : files) {
            Document jsoup = Jsoup.parse(file, "utf-8");
            String name = jsoup.getElementsByTag("img").attr("title");
            Elements element = jsoup.getElementsByTag("p");
            element = element.tagName("scan");
            Message message = new Message();
            message.setUrl("http://www.dianping.com/shop/" + file.getName().substring(0, file.getName().length() - 5));
            message.setName(name.substring(0, name.length() - 3));
            for (Element e : element) {
                String string = e.text().replace(" ", "");
                if (string.contains("地址："))
                    message.setAddress(string);
                if (string.contains("电话："))
                    message.setTel(string);
                if (string.contains("评级："))
                    message.setRank(string);
                if (string.contains("人均："))
                    message.setMoney(string);
            }
            list.add(message);
        }

        File file = new File("D:\\ideaCode\\Crawler\\src\\main\\java\\test\\shopMessage.txt");
        Writer writer = new FileWriter(file);
        for (Message s : list) {
            writer.write(s.toString() + "\n");
        }
        writer.flush();
        writer.close();
    }
}
