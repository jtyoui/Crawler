package cn.tyoui.test;


import cn.tyoui.core.Crawler;
import cn.tyoui.httpclient.HttpCrawler;

/**
 * 测试类
 *
 * @author Tyoui
 */
public class TestCrawler {

    public static void main(String[] args) throws Exception {
        new Crawler().start();
    }

}
