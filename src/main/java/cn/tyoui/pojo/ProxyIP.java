package cn.tyoui.pojo;

/**
 * 代理IP属性类
 *
 * @author Tyoui
 */
public class ProxyIP {
    private String ip;
    private int port;
    private int IPSpeed;

    public ProxyIP(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }


    public int getIPSpeed() {
        return IPSpeed;
    }

    public void setIPSpeed(String IPSpeed) {
        this.IPSpeed = Integer.parseInt(IPSpeed);
    }

    @Override
    public String toString() {
        return ip + ":" + port;
    }
}